import math
import matplotlib.pyplot as plt
from pprint import pprint

dailyI = 0.0065
monthlyI = 1 + (30*dailyI)

def monthlycompound(daily):
    return (daily * 30) + 1

# % gain when winstneming after days days
def winstNeming(dailyInterest, fee, days, investment):
    return (((dailyInterest * days) * investment) - fee) / investment

def findOptimum(dailyInterest, investment, fee):
    # Base case, winstneming after 30 days
    fullArray = []
    optimum = [0, 0]
    # Percentage gain for currently evaluated interval
    currentInterval = 0
    # How often the current interval fits in 30 days
    repeats = 0
    # Percentage gained for month compound interest with current interval
    gain = 0
    for x in range(1,31):
        # Gain after x days
        currentIntervalInterest = winstNeming(dailyInterest, fee, x, investment)
        # Amount of time we can do that in a month
        repeats = 30/x
        gain = ((1+currentIntervalInterest) ** math.floor(repeats)) * (1 + ((repeats - math.floor(repeats)) * currentIntervalInterest))
        fullArray.append(gain)
        if(gain > optimum[1]):
           optimum[0] = x
           optimum[1] = gain
    pprint(fullArray)
    print(f"Compounding every {optimum[0]} days from {investment} investment nets you {(optimum[1]-1) * investment} ADA")
    print(f"Compared to {(monthlycompound(dailyInterest)-1) * investment} ADA from going the full 30 days")
    print(f"For an increase of {(optimum[1] * investment) - (monthlycompound(dailyInterest) * investment)} ADA")
    return fullArray

plt.plot(findOptimum(dailyI, 10000, 0.5))
plt.ylabel('Interest')
plt.show()

print(monthlycompound(dailyI))
